#XBian 1.0 Alpha 5 Changelog
==================================
From the development point of view 
XBian 1.0 Alpha 5 took the most man
hours (we're awaiting feminine team
members). We've completely rewritten
xbian-config and created a custom
implementation for an animated splash.
We also welcomed a new team member 
specialized in python under the name
of belese (and sadly had to say 
goodbye to Hexagon). Belese worked
hard on a xbian-config port inside
XBMC. XBian can now be fully
customized inside XBMC*. Last but not
least, we've finally included XBMC
v12 final.

*Currently only supported under the Confluence skin.

##Bugfixes
==================================
- Issue #88: Make advancedsettings.xml Frodo-proof
- Issue #182: XVID playback stops
- Issue #189: [xbian-config] 4 Packages - mislabelled
- Issue #204: Fix grey blocks in video when skipping/forwarding movie
- Issue #206: Choppy xvid playback - timestamp issue with packed B frames
- Issue #217: Fix problem with 8192cu wireless chip (solution included)
- Issue #222: Switching to next video results in just sound (black screen)
- Issue #225: Out of memory while playing Youtube
- Issue #228: Error: Requested setting (audiooutput.guisoundmode) not found
- Issue #234: Cannot troubleshoot /var/log/messages and kernel not used
- Issue #242: Add swap file (128 MB static size)
- Issue #246: [xbian-config] wlan settings problems with special characters ($)
- Issue #248: Fix (auto)mount issues
- Issue #252: Root not available as source in XBMC (only Fusion available)

##Improvements
==================================
- Issue #10: XBMC Frodo stable
- Issue #125: Splash screen on boot
- Issue #159: Delay when setting audo / Mute doesn't work
- Issue #181: Change cachemembuffersize
- Issue #193: Terratec Cinergy DVB-T Stick Dual Rev 2 recognized as Rev 1
- Issue #195: Update Raspberry Pi firmware
- Issue #212: External HDD Spin down after 5 minutes
- Issue #235: RaspberryPi Crash, have watcher automatically restart PI
- Issue #238 and #243: Remove skin audio settings in confluence
- Issue #239: Add ondemand WOL capability to XBian
- Issue #251: Add TL-WN821N firmware
- Issue #267: Add speedlink remote support

Fixed the black screen on XBMC shutdown, moved (initial) boot script to the initramfs so everything can be setup in a single boot

##XBian-config additions
==================================
- Issue #99: [xbian-config] More standard dialogs
- Issue #110: [xbian-config] Make overclocking a radio dialog and show current settings
- Issue #114: [xbian-config] Make a port for inside XBMC
- Issue #126: Command line control of xbian-config modules
- Issue #142: [xbian-config] All ports XBMC resolution setting
- Issue #143: [xbian-config] Bash Settings like language override and disable autostart
- Issue #144: Integrating xbian-config in the confuence skin
- Issue #194: xbian-config suppresses error messages
- Issue #249: [xbian-config-bash] UTF-8 support for multi-lingual support

##Special thanks to
==================================
deanmv, sraue, flangefrog, ganchito55, namith, mcaptur, s2108312, Kimkos, Nemesis8899
