Text on why it's good to contribute a mirror goes here

## Installing the software

To get a fresh apt mirror up and running, you have to install two things - a webserver and apt-mirror.

### apt-mirror

Apt-mirror is available by default in Debian, Ubuntu and other distributions. If your server is running another distro you'll most probably have to install it manually from http://apt-mirror.sourceforge.net/

    apt-get install apt-mirror

If your distro hasn't got the latest version of apt-mirror installed, you might have to apply a small fix for it to handle armhf packages the way it should. Just to be sure, run this at the command-line (as root)

    sed -i 's/alpha\|amd64\|armel\|arm\|armhf\|hppa/alpha\|amd64\|armel\|armhf\|arm\|hppa/g' /usr/bin/apt-mirror

### Apache 2

If you are on a Debian/Ubuntu based system, apache is installed by entering the following at the command line

    apt-get install apache2

On RedHat based systems (Fedora/CentOS etc.) you use

    yum install httpd


### Cron

cron is installed by default on most systems, so we'll not cover it here.

## Configuration

### apt-mirror

The apt-mirror configuration file is available at /etc/apt/mirror.list , and do only need to contain the following 3 lines

    set nthreads     20
    set _tilde 0
    deb-armhf http://apt.xbian.org wheezy main

You can test your configuration by running

    apt-mirror

... in the command line. If it's successful, you'll end up with a new folder in /var/spool/apt-mirror/mirror/ thats called apt.xbian.org , in there is the now the full XBian apt repo.

Also make sure you copy the key from the apt source and put it in the www root

    wget -P /var/www/ http://apt.xbian.org/xbian.gpg.key

### apache2

apache2 does not need any special configuration, just make sure it's up and running with any of these commands depending on OS. As root:

    service apache2 start      # Ubuntu
    /etc/init.d/apache2 start  # Debian-based
    /etc/init.d/httpd start    # RedHat-based
    
Verify that your server is up by navigating to http://ip-or-hostname-of-server/

### cron

To get this running automatically, you'll have to create/edit /etc/cron.d/apt-mirror

To run mirroring each full hour you would use this

    #
    # Regular cron jobs for the apt-mirror package
    #
    0 * * * *      /usr/bin/apt-mirror > /var/spool/apt-mirror/var/cron.log

And this will make apt-mirror run every day at 03:00

    #
    # Regular cron jobs for the apt-mirror package
    #
    0 3 * * *      /usr/bin/apt-mirror > /var/spool/apt-mirror/var/cron.log

When done, restart the cron service using a method appropriate for your system

## And some symlinks

When everything is up and running, you need to create symlinks from your www-root to the recently created mirror, if you havn't customized anything the following commands will get the job done. As root:

    ln -s /var/spool/apt-mirror/mirror/apt.xbian.org/dists /var/www/
    ln -s /var/spool/apt-mirror/mirror/apt.xbian.org/pool /var/www/

## Manually updating

Sometimes it's necessary to do a manual update of the mirror, thats as easy as entering the following at the command line (again as root)

    apt-mirror

## Directory listing

Make sure you have enabled directory listing in Apache2 so user can browse the different folders, and that only...

    dists
    pool
    xbian.gpg.key

...are listed.

## Contact us

If you done with the mirror, contact us with the specificaties @ development@xbian.org