 Welcome to the official XBian **development** wiki! For the normal wiki see [wiki.xbian.org](http://wiki.xbian.org). You can visit our site at [xbian.org](http://www.xbian.org).

<hr>
**Guides**
* [Compiling XBMC & dependencies](https://github.com/xbianonpi/xbian/wiki/Compiling-XBMC-&-Dependencies)
* [Compiling the Linux kernel & Raspberry Pi firmware](https://github.com/xbianonpi/xbian/wiki/Compiling-the-Linux-kernel-&-Raspberry-Pi-firmware)
* [Compiling Lirc](https://github.com/xbianonpi/xbian/wiki/Compile-Lirc)
* [Creating an image](https://github.com/xbianonpi/xbian/wiki/Creating-an-image)

<hr>
**Important links**
* [Official XBian website](http://www.xbian.org)
* [xbian-config-bash wiki](https://github.com/xbianonpi/xbian-config-bash/wiki)
* [Changelogs] (http://xbian.org/category/news/releases/)

<hr>