SSH into your XBian installation and execute the following commands

```# Make sure all packages are up-to-date & clean APT/DPKG to free up space
sudo su # default password = raspberry
stop xbmc
apt-get update
apt-get upgrade -y
apt-get autoremove -y
apt-get purge -y $(dpkg --get-selections | grep deinstall | awk '{print $1}')
apt-get clean
dpkg --clear-avail
dpkg --configure -a

# Clean XBMC configuration, don't execute this if you want to keep your XBMC configuration
rm -r /home/xbian/.xbmc/media
rm -r /home/xbian/.xbmc/sounds
rm -r /home/xbian/.xbmc/system
rm -r /home/xbian/.xbmc/temp
rm -r /home/xbian/.xbmc/userdata/LCD.xml
rm -r /home/xbian/.xbmc/userdata/Lircmap.xml
rm -r /home/xbian/.xbmc/userdata/RssFeeds.xml
rm -r /home/xbian/.xbmc/userdata/Thumbnails
rm -r /home/xbian/.xbmc/userdata/addon_data
rm -r /home/xbian/.xbmc/userdata/keymaps
rm -r /home/xbian/.xbmc/userdata/library
rm -r /home/xbian/.xbmc/userdata/playlists
rm -r /home/xbian/.xbmc/userdata/profiles.xml

# Link libraries & handle dependency descriptions for loadable kernel modules  
depmod -a
ldconfig

# Manually check /home/xbian for leftovers (like logs) and remove them 
# Manually check /lib/modules for leftovers (for unused kernel versions modules)
# Manually check /etc/apt/sources.list (for unwanted deb sources)

# Remove lirc backup files, remove unwanted files and clear logs
rm -rf /etc/lirc/*xbian*
sudo rm -rf /home/xbian/.xbmc/userdata/addon_data/plugin.xbianconfig/.firstrun
sudo rm /var/log/*.gz
sudo rm /var/log/*log.*
sudo echo "" > /home/xbian/.bash_history
sudo echo "" > /root/.bash_history
rm /var/swapfile

# Stop the system
sudo halt

# Unplug the power now and put your SD card into your computer, for this guide we used a Ubuntu 13.04 virtual machine

# Create a clean directory & mount point
sudo su
cd ~
rm -rf xbian
mkdir xbian
cd ~/xbian
mkdir part2mount

# Setting up the image
dd if=/dev/zero of=xbian.img bs=1M count=1100
#setup bootable FAT32 partition and data
echo "2048,69632,b,*," | sfdisk -u S -N1 -H 4 -S 16 -q xbian.img
echo "71680,+,83,," | sfdisk -u S -N2 -H 4 -S 16 -q xbian.img
# lets map them to loop devices
kpartx -s -l xbian.img -av

#let's copy the FAT32 from any inage on SD card
sudo dd if=/dev/sdc1 of=/dev/loop0p1 bs=512 


mkfs.btrfs -L xbian-root-btrfs /dev/loop0p2
mount -o compress=lzo /dev/loop0p2 part2mount/

# Setting up BTRFS directoy structure & snapshots
btrfs sub create part2mount/root
btrfs sub create part2mount/root/@
btrfs sub create part2mount/home
btrfs sub create part2mount/home/@

# NOTE: Adapt "/media/koenkk/xbian-root-btrfs/root/@/" in the following commands to your own values
rsync -av --progress '/media/koenkk/xbian-root-btrfs/root/@/' 'part2mount/root/@' 
rsync -av --progress '/media/koenkk/xbian-root-btrfs/home/@/' 'part2mount/home/@' 
btrfs sub snap part2mount/root/@ part2mount/root/@safe
btrfs sub snap part2mount/home/@ part2mount/home/@safe

# Resizing the BTRFS filesystem, repeat this until an out of space error appears
btrfs filesystem res -100M part2mount 

# Remember the BTRFS partition size, remember this as [sizefromdmesg]
dmesg | tail 

# Creating part2 of the image, replace [sizefromdmesg] divided by 512 with your result of the previous step
kpartx -d xbian.img
dd if=xbian.img of=xbian.compact.img bs=512 count=[71679 + sizefromdmesgdividedby512]

# Correct the partition table
fdisk xbianimage
# Now press: d, 2, n, p, 2, enter, enter, w, enter

# Done! :) (xbianimage is the resulting image file)
```
