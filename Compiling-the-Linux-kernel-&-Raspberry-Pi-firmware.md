```
#!/bin/bash
#
#Copyright 2012 CurlyMo, Erwin Bovendeur, Koenkk <development@xbian.org>
#
#This file is part of XBian - XBMC on the Raspberry Pi.
#
#XBian is free software: you can redistribute it and/or modify it under the 
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or (at your option) any later 
#version.
#
#XBian is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
#FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
#details.
#
#You should have received a copy of the GNU General Public License along 
#with XBian. If not, see <http://www.gnu.org/licenses/>
#

#We will cross compile on Ubuntu 13.04 to speed up compilation

#Execute everything as root
sudo su

#First we need to install the needed packages
apt-get install gcc-arm-linux-gnueabi make ncurses-dev

#Download compilation tools from the official raspberry pi github
cd /usr/src
git clone --depth 5 https://github.com/raspberrypi/tools.git
cd tools
ln -s /usr/src/tools/arm-bcm2708/arm-bcm2708-linux-gnueabi/bin/arm-bcm2708-linux-gnueabi-gcc /usr/bin/arm-bcm2708-linux-gnueabi-gcc

#Download the official kernel from the Raspberry Pi github
mkdir /opt/raspberry
cd /opt/raspberry
sudo git clone -b rpi-3.9.y --depth 5 git://github.com/raspberrypi/linux.git
cd /opt/raspberry/linux

#Download kernel config and apply patches
wget https://raw.github.com/xbianonpi/xbian-patches/master/kernel/.config
wget https://raw.github.com/xbianonpi/xbian-patches/master/kernel/frandom.patch
wget https://raw.github.com/xbianonpi/xbian-patches/master/kernel/xbian_lirc_kernel.patch
patch -p1 < frandom.patch
patch -p1 < xbian_lirc_kernel.patch

#Make steps with custom commands not supported by the official config
sudo make -j6 ARCH=arm CROSS_COMPILE=/usr/bin/arm-linux-gnueabi-  CONFIG_LIRC_STAGING=y CONFIG_LIRC_RPI=m CONFIG_I2C_DEV=m CONFIG_LIRC_RP1=m CONFIG_LIRC_XBOX=m CONFIG_LIRC_IGORPLUGUSB=m CONFIG_LIRC_BT829=m CONFIG_LIRC_IMON=m CONFIG_LIRC_SASEM=m CONFIG_LIRC_SERIAL=m CONFIG_LIRC_SIR=m CONFIG_LIRC_TTUSBIR=m CONFIG_LIRC_ZILOG=m CONFIG_RT5370STA=m 
sudo make -j6 modules ARCH=arm CROSS_COMPILE=/usr/bin/arm-linux-gnueabi- CONFIG_LIRC_STAGING=y CONFIG_LIRC_RPI=m CONFIG_I2C_DEV=m CONFIG_LIRC_RP1=m CONFIG_LIRC_XBOX=m CONFIG_LIRC_IGORPLUGUSB=m CONFIG_LIRC_BT829=m CONFIG_LIRC_IMON=m CONFIG_LIRC_SASEM=m CONFIG_LIRC_SERIAL=m CONFIG_LIRC_SIR=m CONFIG_LIRC_TTUSBIR=m CONFIG_LIRC_ZILOG=m CONFIG_RT5370STA=m 

# Copy the kernel
mkdir -p /opt/raspberry/kernelandfirmware/boot
sudo make -j6 modules_install ARCH=arm CROSS_COMPILE=/usr/bin/arm-linux-gnueabi- CONFIG_LIRC_STAGING=y CONFIG_LIRC_RPI=m CONFIG_I2C_DEV=m CONFIG_LIRC_RP1=m CONFIG_LIRC_XBOX=m CONFIG_LIRC_IGORPLUGUSB=m CONFIG_LIRC_BT829=m CONFIG_LIRC_IMON=m CONFIG_LIRC_SASEM=m CONFIG_LIRC_SERIAL=m CONFIG_LIRC_SIR=m CONFIG_LIRC_TTUSBIR=m CONFIG_LIRC_ZILOG=m CONFIG_RT5370STA=m INSTALL_MOD_PATH=/opt/raspberry/kernelandfirmware
cp arch/arm/boot/zImage /opt/raspberry/kernelandfirmware/boot/kernel.img

# Prepare the firmware
cd /opt/raspberry
git clone -b next --depth 5 https://github.com/raspberrypi/firmware.git
cd firmware/boot
rm  start_cd.elf start.elf kernel_emergency.img kernel.img fixup_cd.dat fixup.dat LICENCE.broadcom COPYING.linux 
mv start_x.elf start.elf
mv fixup_x.dat fixup.dat
cd ..
rm -rf documentation extra README opt modules
mv hardfp/opt opt
rm -rf opt/vc/src hardfp
cp -R * /opt/raspberry/kernelandfirmware

#Now copy all content of /opt/raspberry/kernelandfirmware to the root filesystem of your XBian installation

### 
### Last step, important:
### On version beta1 and higher, initramfs.gz needs to be properly recreated after kernel/modules update. 
### As a last step (after kernel.img is inside /boot and modules under /lib/modules) run the following 
### command (3.x.y should reflect your new installed kernel version if different from current (output of 
### "uname -r"). version must be one of /lib/modules/*). 
xbian-update-initramfs 3.x.y
```