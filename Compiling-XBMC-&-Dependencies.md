```
#
#Copyright 2012 Koen Kanters, Raspbian
#This file is part of XBian - XBMC on the Raspberry Pi.
#
#XBian is free software: you can redistribute it and/or modify it under the 
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or (at your option) any later 
#version.
#
#XBian is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
#FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
#details.
#
#You should have received a copy of the GNU General Public License along 
#with XBian. If not, see <http://www.gnu.org/licenses/>
#

# NOTES
# - We will be using crosscompiling to reduce compilation time
# - For this guide we used Ubuntu 13.04 64-bit, other distributions should also work but are not tested

# 1. Cloning & setting up the buildfs

  # Execute all steps as super user
  sudo su 

  # Cloning and setting up the repo
  cd /opt
  wget https://downloads.sourceforge.net/project/xbian/development/buildfs.tar.gz?r=&ts=1378574307&use_mirror=freefr -O buildfs.tar.gz
  cp /etc/network/interfaces /opt/buildfs/etc/network/interfaces
  cp /etc/resolv.conf /opt/buildfs/etc/resolv.conf
  mount proc -t proc /opt/buildfs/proc
  chroot /opt/buildfs


# 2. Compiling XBMC
  cd /opt
  
  # If you want to compile XBMC Gotham
  git clone --depth 5 -b master https://github.com/xbmc/xbmc xbmc

  # If you want to compile XBMC Frodo
  git clone -b openelec-Frodo https://github.com/OpenELEC/xbmc.git xbmc
  
  # Patching XBMC
  cd /opt/xbmc
  wget https://raw.github.com/xbianonpi/xbian-patches/master/xbmc/patch-XBMC.sh
  chmod +x patch-XBMC.sh

  # If you want to compile XBMC Gotham replace "12" with "13"
  sh patch-XBMC.sh 12

  # Building XBMC
  sh tools/rbp/setup-sdk.sh
  make -C tools/rbp/depends/xbmc
  make # U can use "Make -j" to compile multiprocessor
  
  # Preparing XBMC binaries
  mkdir /opt/xbmc-bin
  make DESTDIR="/opt/xbmc-bin" install 

  # The XBMC binaries can now be found in /opt/xbmc-bin, you can copy these over to your Raspberry Pi


# 3. Compiling libcec
  cd /opt
  git clone -b master https://github.com/Pulse-Eight/libcec.git libcec
  cd /opt/libcec
  project/RPi/build.sh /usr/local
  
  # Preparing libcec 
  mkdir /opt/libcec-bin
  make DESTDIR="/opt/libcec-bin" install

  # The libcec binaries can now be found in /opt/libcec-bin, you can copy these over to your Raspberry Pi


# 4. Compiling PVR addons
  cd /opt
  git clone git://github.com/opdenkamp/xbmc-pvr-addons.git pvr
  cd /opt/pvr
  export LD=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ld
  export CC=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-gcc
  export CXX=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-g++
  export OBJDUMP=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-objdump
  export RANLIB=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ranlib
  export STRIP=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-strip
  export AR=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ar
  ./bootstrap
  ./configure --prefix=/usr/local --enable-addons-with-dependencies --host=arm-bcm2708hardfp-linux-gnueabi --build=i686-linux

  # Preparing PVR binaries
  mkdir /opt/pvr-bin
  make DESTDIR="/opt/pvr-bin" install
  
  # The PVR binaries can now be found in /opt/pvr-bin, you can copy these over to your Raspberry Pi


# 5. Compiling XVDR addons
  cd /opt
  export LD=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ld
  export CC=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-gcc
  export CXX=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-g++
  export OBJDUMP=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-objdump
  export RANLIB=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ranlib
  export STRIP=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-strip
  export AR=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ar
  git clone git://github.com/pipelka/xbmc-addon-xvdr.git xvdr
  cd /opt/xvdr
  sh autogen.sh
  ./configure --prefix=/usr/local/share/xbmc  --host=arm-bcm2708hardfp-linux-gnueabi --build=i686-linux
   
  # Preparing XVDR binaries
  mkdir /opt/xvdr-bin
  make DESTDIR="/opt/xvdr-bin" install

  # The XVDR binaries can now be found in /opt/xvdr-bin, you can copy these over to your Raspberry Pi


# 6. Compiling shairplay
  cd /opt
  export LD=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ld
  export CC=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-gcc
  export CXX=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-g++
  export OBJDUMP=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-objdump
  export RANLIB=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ranlib
  export STRIP=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-strip
  export AR=/toolchain/arm-bcm2708hardfp-linux-gnueabi/bin/arm-bcm2708hardfp-linux-gnueabi-ar
  git clone https://github.com/juhovh/shairplay.git shairplay
  cd /opt/shairplay
  ./autogen.sh
  ./configure --prefix=/usr  --host=arm-bcm2708hardfp-linux-gnueabi --build=i686-linux
  make
  make DESTDIR="/opt/shairplay-bin" install

  # The XVDR binaries can now be found in /opt/shairplay-bin, you can copy these over to your Raspberry Pi
```