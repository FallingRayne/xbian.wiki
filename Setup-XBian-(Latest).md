```shell
#Copyright 2012 CurlyMo, Koen Kanters, Robin Nilsson, Belese, mk01 <development@xbian.org>
#
#This file is part of XBian - XBMC on the Raspberry Pi.
#
#XBian is free software: you can redistribute it and/or modify it under the 
#terms of the GNU General Public License as published by the Free Software 
#Foundation, either version 3 of the License, or (at your option) any later 
#version.
#
#XBian is distributed in the hope that it will be useful, but WITHOUT ANY 
#WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
#FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
#details.
#
#You should have received a copy of the GNU General Public License along 
#with XBian. If not, see <http://www.gnu.org/licenses/>
#

# For this guide we used the latest XBian image

#Removing unnecessary packages and installing required packages
sudo su

apt-get update

apt-get install -y libtiff-dev liblockdev1-dev wget rpcbind screen git dialog libsamplerate0 \
libmysqlclient18 libsmbclient libssh-4 libavahi-client-dev libmicrohttpd-dev libtinyxml-dev libyajl-dev liblzo2-dev \
libjpeg-dev libfribidi-dev libpcre3-dev libcdio-dev libfreetype6 libpython2.7 libnfs-dev nfs-common samba-common-bin avahi-daemon \
udisks usbmount git-core ntfs-3g wireless-tools libjasper-dev libass-dev fbset libplist1 pv exfat-fuse bc libbluray-dev \
usbutils console-tools libcurl3 hfsplus hfsprogs hfsutils klogd sysklogd libva1 libafpclient0 python-sqlite btrfs-tools gettext --fix-missing

apt-get autoremove -y

apt-get purge -y $(dpkg --get-selections | grep deinstall | awk '{print $1}')

#Setting up xbian user and deleting pi user
useradd -G sudo,input -m -s /bin/bash xbian
echo "xbian:raspberry" | chpasswd

dpkg -L libraspberrypi0 > /home/xbian/list
dpkg -L libraspberrypi-bin >> /home/xbian/list
dpkg -L raspberrypi-bootloader >> /home/xbian/list

for LINE in $(cat /home/xbian/list); do 
    if [ -f $LINE ]; then 
        if [ -f /home/xbian/raspberry.tar ]; then
            tar rvf /home/xbian/raspberry.tar $LINE
        else
            tar cvf /home/xbian/raspberry.tar $LINE
        fi
    fi 
done;

apt-get purge libraspberrypi0 libraspberrypi-bin libraspberrypi-dev raspberrypi-bootloader -y
tar -xvf /home/xbian/raspberry.tar -C /
rm /home/xbian/raspberry.tar
rm /home/xbian/list
ldconfig

rm -rf /var/cache/apt/archives/*

apt-get clean

#Regenerating SSH unique SSH keys:
rm /etc/ssh/ssh_host_* && dpkg-reconfigure openssh-server

#Remove user pi from sudoers and add xbian user
sed -i 's/pi ALL=(ALL) NOPASSWD: ALL//g' /etc/sudoers
echo "xbian ALL=(ALL) NOPASSWD: /usr/local/sbin/xbian-config, /sbin/halt, /sbin/reboot, /usr/bin/splash" >> /etc/sudoers

# Relogin with ssh; user:xbian pass:raspberry

#Kill all processes started by user pi
echo raspberry | sudo -S chmod +s /bin/su
sudo su
for i in $(pgrep -u pi); do kill -9 $i; done;

#Deleting user pi
userdel pi
rm -rf /home/pi

#Disabling root login
sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config

#Set hostname
echo "xbian" > /etc/hostname
hostname xbian
sed -i 's/raspberrypi/xbian/g' /etc/hosts

#Clone xbian git
cd /home/xbian/
git clone --depth 5 -b xbian-beta1 https://github.com/xbianonpi/xbian.git source
cd source

#Installation
cp -R etc/* /etc/
cp -R usr/* /usr/
rm -rf /lib/modules/*
cp -R lib/* /lib/
rm -rf /boot/*
cp -R boot/* /boot/
cp -R home/* /home/
rm -rf /opt/vc
cp -r opt/vc /opt
cp -R var/* /var/

rm /etc/profile.d/raspi-config.sh

# Correcting permissions
chmod +x /etc/init.d/*
chmod +x /usr/local/sbin/*
chmod +x /usr/local/bin/*
chmod +x /usr/local/lib/xbmc/xbmc.bin
chmod +x /opt/vc/bin/*
chmod +x /opt/vc/sbin/*
chown -R xbian:xbian /home/xbian/.xbmc

#Update rc.d
update-rc.d bootlocal defaults
update-rc.d xbmc defaults
update-rc.d lirc defaults
update-rc.d rpcbind defaults
update-rc.d klogd remove
update-rc.d sysklogd remove
update-rc.d zram-swap defaults

#Fixing broken symbolic links
rm /usr/local/lib/libtag_c.so.0
ln -s /usr/local/lib/libtag_c.so.0.0.0 /usr/local/lib/libtag_c.so.0
rm /usr/local/lib/libshairport.so.0
ln -s /usr/local/lib/libshairport.so.0.0.0 /usr/local/lib/libshairport.so.0
rm /usr/local/lib/libtag.so.1
ln -s /usr/local/lib/libtag.so.1.12.0 /usr/local/lib/libtag.so.1
ln -s /usr/local/lib/liblirc_client.so.0.2.1 /usr/local/lib/liblirc_client.so.0
ln -s /usr/local/lib/liblirc_client.so.0.2.1 /usr/local/lib/liblirc_client.so
ln -s /opt/vc/lib /usr/lib/vfp

ldconfig

# Set locale to c to prevent weird characters in xbian-config and c compiling
sed -i 's/# en_GB.UTF-8 UTF-8/en_GB.UTF-8 UTF-8/g' /etc/locale.gen
locale-gen

#Login as user xbian and set locates and as user root
sed -i 's/    SendEnv LANG LC_\*/#   SendEnv LANG LC_\*/g' /etc/ssh/ssh_config

export LANG=C.UTF-8
export LC_ALL=C.UTF-8
export LANGUAGE=C.UTF-8

#Make locale settings permanent
sed -i 's/export PATH/export PATH\nexport LANG=C.UTF-8\nexport LC_ALL=C.UTF-8\nexport LANGUAGE=C.UTF-8/g' /etc/profile
#Allow UTF-8 output
sed -i "s/export PATH/echo -ne '\\\e%G\\\e[?47h\\\e%G\\\e[?47l'\nexport PATH/g" /etc/profile 

#Enable external HDD spindown (5 minutes)
echo "/dev/sda {
    spindown_time = 60
}
" >> /etc/hdparm.conf
echo "/dev/sdb {
    spindown_time = 60
}" >> /etc/hdparm.conf

#Enable auto fsck on boot
sudo sed -i 's/#FSCKFIX=no/FSCKFIX=yes/g' /etc/default/rcS && grep "FSCKFIX=yes" /etc/default/rcS | wc -l

#After the reboot execute this command
sudo su
depmod -a

#Clear logs
rm /var/log/*.gz
rm /var/log/*log.*
for logs in `find /var/log -type f`; do echo '' > $logs; done
echo "" > /root/.bash_history
echo "" > /home/xbian/.bash_history

#Removing swap file & XBian repo clone
rm /var/swap
rm -rf /home/xbian/source

#Add apt sources
echo "deb mirror://apt.xbian.org/mirror.txt wheezy main" >> /etc/apt/sources.list
wget -O - http://apt.xbian.org/xbian.gpg.key | apt-key add -
apt-get update 
apt-get install -y xbian-update
apt-get autoremove -y

echo "evdev" >> /etc/modules
echo "frandom" >> /etc/modules

mv /dev/urandom /dev/urandom.orig
ln -s /dev/frandom /dev/urandom

mv /dev/random /dev/random.orig
ln -s /dev/erandom /dev/random

echo 'man-db man-db/auto-update boolean false' |sudo debconf-set-selections

echo 'APT::Update::Post-Invoke-Success {"touch /var/lib/apt/periodic/update-success-stamp 2>/dev/null || true";};' > /etc/apt/apt.conf.d/15update-stamp
echo 'Acquire::http::Timeout "10"' > /etc/apt/apt.conf.d/10AptTimeout

sed -i 's/WPA_SUP_PIDFILE="\/var\/run\//WPA_SUP_PIDFILE="\/root\//g' /etc/wpa_supplicant/functions.sh

chown xbian:xbian /home/xbian/.bashrc

#Resize SD upon next boot
update-rc.d firstrun defaults

history -c
```